class Main {
    public static void main(String[] args) {

        /* ÜBUNG 1 */

        // Aufgabe 1.1

        System.out.println("Übung 1 Aufgabe 1.1\n\n");

        System.out.print("Das ist ein Beispielsatz.");
        System.out.print("Ein Beispielsatz ist das");

        // Aufgabe 1.2

        System.out.println("\n\nÜbung 1 Aufgabe 1.2\n\n");

        System.out.println("Das ist ein \"Beispielsatz\".");
        System.out.println("Ein Beispielsatz ist das");

        // Aufgabe 1.3

        System.out.println("\n\nÜbung 1 Aufgabe 1.3\n\n");

        // Das ist ein Kommentar.

        // Aufgabe 2.0

        System.out.println("\n\nÜbung 1 Aufgabe 2\n\n");

        int max_length = 13;

        String stars = "*************";
        for (int i = 0; i <= max_length / 2; i++) {
            System.out.printf(
                    "%" + Integer.toString((max_length / 2) + i + 1) + "." + Integer.toString(i * 2 + 1) + "s\n",
                    stars);

        }

        for (int i = 0; i < 2; i++) {
            System.out.printf("%" + Integer.toString((max_length / 2) + 2) + ".3s\n", stars);
        }

        // Aufgabe 3

        System.out.println("\n\nÜbung 1 Aufgabe 3\n\n");

        double[] zahlen = new double[] { 22.4234234, 111.2222, 4.0, 1000000.551, 97.34 };

        for (double zahl : zahlen)
            System.out.printf("%.2f\n", zahl);

        /* ÜBUNG 2 */

        // Aufgabe 1

        System.out.println("\n\nÜbung 2 Aufgabe 1\n\n");

        System.out.printf("%6.2s\n", stars);
        for (int i = 0; i < 2; i++)
            System.out.printf("%.2s\t%.2s\n", stars, stars);
        System.out.printf("%6.2s\n", stars);

        // Aufgabe 2

        System.out.println("\n\nÜbung 2 Aufgabe 2\n\n");

        System.out.printf("%-5s = %-19s = %4s\n", "0!", "", "1");
        System.out.printf("%-5s = %-19s = %4s\n", "1!", "1", "1");
        System.out.printf("%-5s = %-19s = %4s\n", "2!", "1 * 2", "2");
        System.out.printf("%-5s = %-19s = %4s\n", "3!", "1 * 2 * 3", "6");
        System.out.printf("%-5s = %-19s = %4s\n", "4!", "1 * 2 * 3 * 4", "24");
        System.out.printf("%-5s = %-19s = %4s\n", "5!", "1 * 2 * 3 * 4 * 5", "120");

        // Aufgabe 3

        System.out.println("\n\nÜbung 2 Aufgabe 3\n\n");

        String fahrenheit[] = new String[] { "-20", "-10", "+0", "+20", "+30" };
        double celsius[] = new double[] { -28.8889, -23.3333, -17.7778, -6.6667, -1.1111 };

        System.out.printf("%-12s | %10s", "Fahrenheit", "Celsius\n");
        System.out.println("-------------------------");

        for (int i = 0; i < fahrenheit.length; i++) {

            System.out.printf("%-12s | %10s\n", fahrenheit[i], celsius[i]);
        }
    }
}